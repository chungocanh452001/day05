<?php
    session_start();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="register.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    </head>
    <body>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div class="container">
                <div class="container__content">
                    <div class="item">
                        <label for="name" class="flabel">
                            Họ và tên
                        </label>
                        <div class="form__value">
                            <?php echo $_SESSION['name'] ?>
                        </div>
                    </div>
                    <div class="item">
                        <div class="flabel">
                            Giới tính
                        </div>
                        <div class="form__value">
                            <?php echo $_SESSION['gender'] ?>
                        </div>
                    </div>
                    <div class="item">
                        <label for="select" class="flabel">
                            Phân khoa
                        </label>
                        <div class="form__value">
                            <?php echo $_SESSION['select'] ?>
                        </div>
                    </div>
                    <div class="item">
                        <label for="birthday" class="flabel">
                            Ngày sinh
                        </label>
                        <div class="form__value">
                            <?php echo $_SESSION['birthday'] ?>
                        </div>
                    </div>
                    <div class="item">
                        <label for="address" class="flabel">
                            Địa chỉ
                        </label>
                        <div class="form__value">
                            <?php echo $_SESSION['address'] ?>
                            
                        </div>
                    </div>
                    <div class="item">
                        <label for="image" class="flabel">Hình ảnh</label>
                        <div class="form__value fomr__image">
                            <img class="image" src="<?php echo $_SESSION['file'] ?>" alt="">                            
                        </div>
                    </div>
        
                    <input type="submit" value="Xác nhận" class="form__submit">    
                </div>
            </div>
        </form>
    </body>
</html>
